/**
 * @file
 * Order timeout javascript functions.
 *
 * Poll a Drupal site via AJAX at a specified interval.
 */

(function ($) {

  Drupal.behaviors.CommerceCheckoutTimeout = {
    attach: function (context, settings) {
      $.get('/commerce-checkout-timeout/check', function(data) {
        if (data === '1') {
          var redirectUrl = settings.CommerceCheckoutTimeout.timeoutRedirectUrl;
          window.location = redirectUrl;
        }
      });
    }
  };

}(jQuery));
