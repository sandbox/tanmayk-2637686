<?php

/**
 * @file
 * Administrative callbacks for Checkout timeout module.
 */

/**
 * Checkout timeout configuration form.
 */
function commerce_checkout_timeout_config_form($form, &$form_state) {
  $form = array();
  $form['commerce_checkout_timeout_interval'] = array(
    '#title' => t('Timeout interval'),
    '#type' => 'select',
    '#options' => commerce_checkout_timeout_interval_options(),
    '#default_value' => variable_get('commerce_checkout_timeout_interval', COMMERCE_CHECKOUT_TIMEOUT_INTERVAL),
  );
  $form['commerce_checkout_timeout_redirect_url'] = array(
    '#title' => t('Redirect URL'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_checkout_timeout_redirect_url', '<front>'),
    '#description' => t('Redirect url where user will be redirected after checkout timeout.'),
  );

  // TODO Add checkboxes for both messages.
  // Messages.
  // TODO Display below message when product is added to cart and only once.
  $form['commerce_checkout_timeout_warning'] = array(
    '#title' => t('Timeout warning message'),
    '#type' => 'textfield',
    '#size' => 100,
    '#description' => t('Message displayed when product is added to cart. This message notifies user about order timeout. Use !n to display time in message, e.g. !n will be displayed as 5 minutes. <strong>Not working right now.</strong>'),
    '#default_value' => variable_get('commerce_checkout_timeout_warning', 'Your booking will be reserved for !n.'),
  );

  $form['commerce_checkout_timeout_message'] = array(
    '#title' => t('Timeout message'),
    '#type' => 'textfield',
    '#size' => 100,
    '#description' => t('Message displayed when order is emptied.'),
    '#default_value' => variable_get('commerce_checkout_timeout_message', 'It looks like it took too much time to complete the checkout so your order has expired, please proceed with fresh checkout.'),
  );
  return system_settings_form($form);
}
